# CUDA Optimization Algorithms
This repository contains a collection of CUDA-based algorithms that can be used to solve optimization problems.

## Prerequisites
To build and run the algorithms in this repository, you will need:
- [ ] A CUDA-compatible GPU
- [ ] The NVIDIA CUDA Toolkit, which can be downloaded from the [NVIDIA website](https://developer.nvidia.com/cuda-toolkit)
- [ ] A CUDA compatible C++ compiler that supports C++11 or newer ([NVCC](https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/))


## Building the algorithms
To build the algorithms, simply run make in the root directory of the repository. This will build all of the algorithms and create a series of executables that you can run to test the algorithms.

## Running the algorithms
To run the algorithms, simply execute the corresponding executable. For example, to run the Genetic Algorithm, you can use the following command:
```
./ga
```

You can specify command-line arguments to control the behavior of the algorithms. For example, to change the number of generations for the Genetic Algorithm, you can use the following command:
```
./ga --generations 100
```

## Algorithms
The repository currently contains the following algorithms:
- [ ] Genetic Algorithm: A heuristic optimization algorithm inspired by natural evolution. Can be used to find approximate solutions to complex optimization problems.

## License
The algorithms in this repository are released under the [MIT license](https://opensource.org/licenses/MIT).

## Acknowledgements
The algorithms in this repository were developed using the [NVIDIA CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit).