#include <cmath>
#include <iostream>
#include <omp.h>

// define the himmelbleau equation
double himmelbleau(double x, double y) {
  return pow(pow(x, 2) + y - 11, 2) + pow(x + pow(y, 2) - 7, 2);
}

// define the finite difference method to estimate the gradient
std::pair<double, double> finite_difference(double x, double y, double eps = 1e-5) {
  double der_x = (himmelbleau(x + eps, y) - himmelbleau(x, y)) / eps;
  double der_y = (himmelbleau(x, y + eps) - himmelbleau(x, y)) / eps;
  return {der_x, der_y};
}

// define the SGD optimizer
std::pair<double, double> sgd_optimizer(double x, double y, double learning_rate = 0.01, int iterations = 1000) {
  // loop through the number of iterations
  #pragma omp parallel for
  for (int i = 0; i < iterations; i++) {
    // calculate the gradient using finite differences
    auto [der_x, der_y] = finite_difference(x, y);
    // update the parameters
    x -= learning_rate * der_x;
    y -= learning_rate * der_y;
  }
  // return the optimized parameters
  return {x, y};
}

int main() {
  // set the initial parameters
  double x = 0;
  double y = 0;

  // run the optimizer
  auto [x_opt, y_opt] = sgd_optimizer(x, y, 0.01, 1000);

  // print the optimized parameters
  std::cout << "Optimized parameters: x=" << x_opt << ", y=" << y_opt << std::endl;

  // print the optimized value of himmelbleau equation
  std::cout << "Optimized value of himmelbleau equation: " << himmelbleau(x_opt, y_opt) << std::endl;

  return 0;
}
